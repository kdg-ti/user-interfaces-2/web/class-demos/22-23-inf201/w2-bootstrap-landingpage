import 'bootstrap' // importeer bootstrap JavaScript code

import 'bootstrap/dist/css/bootstrap.css' // importeer bootstrap CSS code
import 'bootstrap-icons/font/bootstrap-icons.css' // importeer bootstrap ICONS CSS code

import {Popover, Tooltip} from "bootstrap";


const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
console.log(popoverTriggerList);

for (let popoverelement of popoverTriggerList) {
    new Popover(popoverelement);
}

const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')

for (let tooltipElement of tooltipTriggerList) {
    new Tooltip(tooltipElement)
}
